---
layout: post
title: "A few new tensor toolboxes"
author: Jeremy Cohen
category: Discussions
---

There has been a few additions to [the list of tensor toolboxes]( {{
site.baseurl }}/toolboxes.html ): Hottbox, TensorToolbox.jl. Here is a quick words about these interesting softwares.

<!--more-->

## TensorToobox.jl (julia)

For thoses who want to start working with the now quite mature [Julia](https://julialang.org/) programming language, there has been recently [a new toolbox](https://github.com/lanaperisa/TensorToolbox.jl) released in Daniel Kressner's team, that mimicks the well-known tensor toolbox on matlab. It has the particularity to handle tensor train formats.

Care however that Julia is a fast changing language, so keep updated for various changes in the syntax.


## Hottbox (python)

A team of researchers at the Imperial College of London have recently been
publishing several papers related to classification with tensor data and
models. They have now released their own toolbox in Python, [HOTTBOX](https://github.com/hottbox/hottbox), which
interestingly contains several tools for getting started easily with such
classification problems. 


