---
layout: post
title: "Changes to the website"
author: Jeremy Cohen
category: News
---

It has been roughly one year since the last news on this website. Here are some
of the changes that have implemented recently, in the hope to center the scope
of tensorly towards useful information for everyone.

## Simplying the website

As you will see, there are fewer sections, and fewer text. I have kept the
important content for now: the list of toolboxes (which has been updated, check
next post).

## Adding a list of todo

There is a lot to do to make tensorworld a truly useful place for researchers
to find information about tensors and tensor research. I ahve listed a few
priorities in "The project" section, go and have a look if you might be able to
contribute.

<!--more-->

